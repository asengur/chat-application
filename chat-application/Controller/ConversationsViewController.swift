 //
//  ConversationsViewController.swift
//  chat-application
//
//  Created by Ali Şengür on 27.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import FirebaseAuth
 

class ConversationsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        validateAuth()
    }
    
    private func validateAuth() {
        if Auth.auth().currentUser == nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loginVC = storyboard.instantiateViewController(identifier: "LoginViewController")
            self.present(loginVC, animated: true, completion: nil)
        }
    }

    
    
    @IBAction func didTappedComposeButton(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyboard.instantiateViewController(identifier: "NewConversationViewController") as? NewConversationViewController {
            vc.completion = { [weak self] result in
                print(result)
                self?.createNewConversation(result: result )
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    
    private func createNewConversation(result: [String: String]) {
        
        guard let name = result["name"], let email = result["email"] else { return }

        let chatVC = ChatViewController(with: email)
        chatVC.isNewConversation = true
        chatVC.title = name
        chatVC.navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.pushViewController(chatVC, animated: true)
        
    }
    
    
 }
 
 
 
 extension ConversationsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = "Hello world!"
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let chatVC = storyboard.instantiateViewController(identifier: "ChatViewController") as? ChatViewController {
            self.navigationController?.pushViewController(chatVC, animated: true)
        }
    }
    
    
 }
