//
//  LoginViewController.swift
//  chat-application
//
//  Created by Ali Şengür on 27.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import FirebaseAuth


class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var createAccountButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        loginButton.layer.cornerRadius = 6
        createAccountButton.layer.cornerRadius = 6
        emailTextField.becomeFirstResponder()
    }
    

    @IBAction func didTappedLoginButton(_ sender: UIButton) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        
        guard let email = emailTextField.text, !email.isEmpty,
            let password = passwordTextField.text, !email.isEmpty, password.count > 6 else {
                alertLoginError()
                return }
        
        Auth.auth().signIn(withEmail: email, password: password) {(authResult, error) in
            guard let result = authResult, error == nil else {
                debugPrint("Failed to log in user wih email : \(email)")
                return
            }
            
            UserDefaults.standard.set(email, forKey: "email")
            
            let user = result.user
            print("Logged in user . \(user)")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let mainVC = storyboard.instantiateViewController(identifier: "TabBarController") as? UITabBarController {
                self.present(mainVC, animated: true, completion: nil)
            }
        }
        
    }
    
    
    func alertLoginError() {
        let alert = UIAlertController(title: "Error", message: "Please enter all information to create new account", preferredStyle: .alert)
        let dismissAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        alert.addAction(dismissAction)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func didTappedCreateAccountButton(_ sender: UIButton) {
        
        
    }
}
